#!/usr/bin/env python3
# coding: utf-8

#TODO: auto recogn decimal sep
#TODO: fix requirements.txt [x]

import argparse
import sys
import os
from pathlib import Path
""" import logging

class LogFile(object):
    #File-like object to log text using the `logging` module

    def __init__(self, name=None):
        self.logger = logging.getLogger(name)

    def write(self, msg, level=logging.INFO):
        self.logger.log(level, msg)

    def flush(self):
        for handler in self.logger.handlers:
            handler.flush()

logging.basicConfig(level=logging.DEBUG, filename='mylog.log')

# Redirect stdout and stderr
sys.stdout = LogFile('stdout')
sys.stderr = LogFile('stderr')

print("hello")
"""

class Logger_stdout(object):
    def __init__(self):
        self.terminal = sys.stdout
        self.log = open("logs.log", "w")

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)

    def flush(self):
        #python 3 compatibility.
        pass

class Logger_stderr(object):
    def __init__(self):
        self.terminal = sys.stderr
        self.log = open("error_logs.log", "w")

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)

    def flush(self):
        #python 3 compatibility.
        pass


sys.stdout = Logger_stdout()
sys.stderr = Logger_stderr()

print("hello")

#option parser
parser = argparse.ArgumentParser(description="Split the raw data file into n output files containing each single raman spectra (in the format [raman_shift <tab> intensity]). \nAt the first usage, add the --requirements option to auto-install dependencies (from the provided 'requirements.txt' file. ", formatter_class=argparse.RawTextHelpFormatter)

parser.add_argument('--input_file', type=str, help="input file path </path/to/your_inputfile.txt>, \nor simply <your_inputfile.txt> if located in the same directory where the script is")
parser.add_argument('--output_file', type=str, help="output file path \n(the i-th file will be renamed as i_<outputfile>.txt)")
parser.add_argument("--verbose", help="increase output verbosity",action="store_true")
parser.add_argument('--decimals', type=str, help=' "," or "." as decimal separator (i.e. 500.999 vs 500,999)\n(default: ",") ')

#parser.add_argument('--requirements', help="use this option to install the needed requirements \n(needed only the first time the script is exec!)",action="store_true")

try:
	import easygui as g
except:
	print("installing your graphic interface (network needed)")
	os.system('python -m pip install easygui')
	import easygui as g

#important this module to be in the same dir of *_exe.py
from easygui_timerbox import timerbox

if __name__ == '__main__':
	#parsing args
	args = parser.parse_args(sys.argv[1:])

	if args.verbose:
		print("using verbose mode")
		verbose=True
	else:
		result=timerbox('Verbose execution? \n(defualt: non verbose)', 'Verbosity', choices=['False', 'True'], time=1)
		if result=="True":
			verbose=True
		else:
			verbose=False
	
	# Requirements no longer needed in standalone version, be sure to have those installed while exe-compiling
	"""
	if args.requirements:
		if verbose:
			print("installing requirements")
		os.system('python -m pip install -r requirements.txt --user')
	else:
		if verbose:
			msg1 = "Do you want to install requirements? \nPress Continue if it's the first time you run the script on the current machine"
			title1 = "Please Confirm"
			if g.ccbox(msg1, title1, choices=("C[o]ntinue", "[S]kip")):     # show a Continue/Cancel dialog
				print("installing requirements")
				os.system('python -m pip install -r requirements.txt --user')
			else:  # user chose Cancel
				pass
		else:
			#print("not installing requirements (not the first run?)")
		    pass
		pass
	"""
	
	if args.input_file:
		__pathinfile__=Path(args.input_file)
	else:
		__pathinfile__=Path(g.fileopenbox(msg="Select your input file"))
		#print("No input file provided, please type\n > split_rawdata.py -h \nfor help. \nExiting...")
		#sys.exit()
		#__pathinfile__

	if args.output_file:
		__pathoutfile__=Path(args.output_file)
		if os.path.isabs(__pathoutfile__):
			absolute_path=True
		else:
			absolute_path=False
	else:
		__pathoutfile__=Path(g.enterbox("Type the desired output file name:"))
		dir_path=__pathinfile__.parent / Path("output")
		if not os.path.exists(dir_path):
		    print("creating the {} folder".format(str(dir_path)))
		#os.system("mkdir {}/output".format(str(__pathinfile__.parent)))
		    os.mkdir(dir_path)
		s.__pathoutfile__= dir_path / __pathoutfile__
		#__pathoutfile__
		absolute_path=True
		#print("No output file provided, please type\n > split_rawdata.py -h \nfor help. \nExiting...")
		#sys.exit()
	
	if verbose:
		print("####\ninput file: {} \noutput file: {}\n####".format(__pathinfile__, __pathoutfile__))

	#parsing the decimal separator in the output format
	if args.decimals:
		if "," in args.decimals:
			decimals=","
		elif "." in args.decimals:
			decimals="."
		else:
			print("unrecognized option, exiting")
			sys.exit()
		if verbose:
			    print('using "{}" for decimal'.format(decimals))
	else:
		if verbose:
			print('using "," for decimal')
		decimals=","

	#processing starts here
	import pandas as pd

	print("Reading file: ", __pathinfile__,"...",end="" )
	raw_data=pd.read_csv(__pathinfile__, decimal=",", sep="\t", header=None)
	print("done.")
	if verbose:
		from pprint import pprint
		print("Quick overview (first 5-lines of {})". format(__pathinfile__))
		pprint(raw_data.head())

	#splitting
	#assuming the 0-th column always contains the raman shift
	r_shift=raw_data[0]

	for i in range(1,len(raw_data.columns)):
		curr_intensity=raw_data[i]
		if absolute_path:
			name=__pathoutfile__.name
			parent=__pathoutfile__.parent
			output_path=Path(str(parent)+ "/{}_".format(i)+str(name))
		else:
			output_path=Path(str(i)+"_"+str(__pathoutfile__))
		if verbose:
			print("saving {}-th\tspectra as {} ...".format(i,output_path), end="")
		pd.concat([r_shift, curr_intensity], axis=1).to_csv(output_path, sep="\t",index=False, header=None, decimal=decimals)
		if verbose:
			print("done.")
