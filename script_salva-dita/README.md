# split_rawdata usage

### Descrizione: 

  - lo script prende in input il file "raw", assumendo che la prima colonna sia il raman-shift, e le altre colonne corrispondano ciascuna a uno spettro (intensità).
  
    Il file di input non viene alterato dallo script.

  - restituisce un file per ogni colonna di spettri, contenete per ciascuna riga la coppia [raman shift (tab) spettro].
    I file vengono rinominati con un nome scelto dall'utente, e posizionati nella cartella "output" all'interno della stessa directory del file di partenza

#### Interfaccia Grafica

  - per usare lo script con "interfaccia grafica" sotto Windows, scaricare il file [split_rawdata.exe](https://gitlab.com/DBertazioli/scripts/blob/master/script_salva-dita/dist/split_rawdata.exe) e il file  [requirements.txt](https://gitlab.com/DBertazioli/scripts/blob/master/script_salva-dita/dist/requirements.txt) e salvarli nella stessa cartella (una cartella qualunque)
  
  - eseguire il file [split_rawdata.exe](https://gitlab.com/DBertazioli/scripts/blob/master/script_salva-dita/dist/split_rawdata.exe) (già compilato per python3.6 o superiore (da verificare)) e seguire le istruzioni man mano che compaiono.
    
  -  Alla prima run, consentire l'installazione dei "requirements": è fondamentale che il file [requirements.txt](https://gitlab.com/DBertazioli/scripts/blob/master/script_salva-dita/dist/requirements.txt) si trovi nella stessa cartella dell'eseguibile.
    
  - Non è invece necessario che i file di input (e di output) si trovino nella medesima cartella. In particolare, i file di output verranno salvati nella medesima cartella del file di input, creando una sottocartella di nome "output"
  
    nota: Nella cartella dove è situato l'eseguibile, si creano due file di log ("logs.log" ed "error_logs.log"), all'interno dei quali vengono salvati rispettivamente eventuali output verbali ed errori.

#### Terminale
  - lo script eseguibile da linea di comando è il seguente: [split_rawdata.py](https://gitlab.com/DBertazioli/scripts/blob/master/script_salva-dita/split_rawdata.py)

  - da linea di comando, per visualizzare l'help:
      ```>python split_rawdata.py -h```
  
  - comando completo di esempio:
    
      ```>python split_rawdata.py --input_file Iniziale.txt --output_file Finale.txt --verbose```
      
    se il file di input è nella stessa cartella del file, altrimenti occorrerà aggiungere a "Iniziale.txt" il percorso per quel file (C:...Iniziale.txt, same per il file di output).
    
    La flag --verbose si può omettere per un output meno verboso (meno descrizioni)
    
    
  - alla prima esecuzione, eseguire anche la flag "--requirements" per installare le dipendenze:
    
      ```>python split_rawdata.py --input_file Iniziale.txt --output_file Finale.txt --verbose --requirements```
      
    collocare il file "requirements.txt" nella stessa cartella in cui è lanciato il python script! 
      
  - note: usare la flag "--decimals" per settare il carattere separatore delle cifre decimali ( vedasi l'help)
    
  

    
